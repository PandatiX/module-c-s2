<div align="center">
    <img src="res/logo.png"/>
    <h1>Module C S2</h1>
</div>

# Module C S2

## Details

[Module C S2](https://gitlab.com/PandatiX/module-c-s2) is the C module
project for the 2nd semester at the ENSIBS of:
- Lucas TESSON, e2004675 ;
- Steven CARRIER, e2004742.

[![pipeline status](https://gitlab.com/PandatiX/module-c-s2/badges/master/pipeline.svg)](https://gitlab.com/PandatiX/module-c-s2/-/commits/master)

## How to compile

To compile every TP's binary, run `make` or `make all`.

To clean TPs' binaries, run `make clean`.

Because we had to create only 2 Makefile, one to compile binaries and
one to recursively call the previous one, the wanted dir struct could
not be perfectly respected. Because of this, each projet/exercise has
its own project struct as follows.

```
├── Makefile
├── src
|   └── ...
└── bin
    └── arch
        └── prog.exe
```

Notice you can run TP3/degres unit tests with `make test`. This makes a
3rd Makefile, we know it...
To avoid other TPs to fail on this, do the following.

```bash
pushd TP3/degres
make test -B
```

The root Makefile is slightly modified from the other "root" Makefiles
because it must try not to go in the `res` directory.
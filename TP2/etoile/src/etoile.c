#include <stdio.h>
#include <stdlib.h>

void drawSquare(int size) {
    int rows, columns;
    rows = 1;
    //Tant que l'on atteint pas la hauteur souhaitee, on inscrit une etoile
    while(rows <= size) {
        columns = 1;
        //Tant que l'on atteint pas la largeur souhaitee, on inscrit une etoile
        while(columns <= size) {
            printf("*");
            columns++;
        }
        printf("\n");
        rows++;
    }
    printf("\n");
}

void drawRightTriangle(int size) {
    int rows, columns;
    //Pour la taile de la fleche souhaitee, on incremente le nombre d'etoile de 1 a chaque ligne
    for(rows = 1; rows <= size; rows++) {
        for(columns = 1; columns <= rows; columns++) {
            printf("*");
        }
        printf("\n");
    }
    /* Pour la taile de la fleche souhaitee - 1 pour ne pas avoir deux lignes avec un nombre d'etoile egale a la taille,
    on decremente le nombre d'etoile de 1 a chaque ligne*/
    for(rows = 1; rows <= size - 1; rows++) {
        for(columns = rows; columns <= size - 1; columns++) {
            printf("*");
        }
        printf("\n");
    }
    printf("\n");
}

void drawLeftTriangle(int size) {
    int rows, columns;
    //Pour la taile de la fleche souhaitee, on decremente de 1 le nombre d'espace puis on incremente le nombre d'etoile ensuite
    for(rows = 1; rows <= size; rows++) {
        for(columns = 1; columns <= size - rows; columns++) {
            printf(" ");
        }
        for(columns = 1; columns <= rows; columns++) {
            printf("*");
        }
        printf("\n");
    }
    //Etape inverse de la boucle precedente en partant de la taille - 1 pour eviter un doublon dans les lignes
    for(rows = size - 1; rows > 0; rows--) {
        for(columns = 1; columns <= size - rows; columns++) {
            printf(" ");
        }
        for(columns = 1; columns <= rows; columns++) {
            printf("*");
        }
        printf("\n");
    }
    printf("\n");
}

void drawTriangle(int size) {
    int rows, columns;
    int tmp = 0;
    /* Pour la taile de la fleche souhaitee, on decremente de 1 le nombre d'espace 
    puis on incremente le nombre d'etoile ensuite tant qu'on atteint pas la limite de la taille souhaitee en largeur*/
    for (rows = 1; rows <= size; ++rows, tmp = 0) {
        for (columns = 1; columns <= size - rows; ++columns) {
            printf(" ");
        }
        while (tmp != 2 * rows - 1) {
            printf("*");
            ++tmp;
        }
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char** argv) {
    int size;
    //On verifie que l'on a passe un seul argument au programme
    if(argc == 2) {
        size = atoi(argv[1]);
        drawSquare(size);
        drawRightTriangle(size);
        drawLeftTriangle(size);
        drawTriangle(size);
    }
    else if(argc > 2) {
        printf("Veuillez indiquer seulement la taille en parametre\n");
    }
    else {
        printf("Merci d'indiquer la taille souhaitee.\n");
    }
    return 0;
}
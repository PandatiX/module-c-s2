#include <stdio.h>
#include <stdlib.h> 
#include <fcntl.h>
#include <unistd.h>

#define N 30

int getRandomNumber(int min, int max) {
    int num = (rand() % (min - max + 1)) + min; 
    return num;
}

void triBulle(int tab[N]) {
    // On parcours la liste jusqu'a l'avant dernier element
    for(int i = (N - 2); i >= 0; i--) {
        // Pour chaque element on compare l'element avec le suivant et les permute si besoin
        for(int j = 0; j <= i; j++) {
            if(tab[j + 1] < tab[j]) {
                int t = tab[j + 1];
                tab[j + 1] = tab[j];
                tab[j] = t;
            }
        }
    }
    // Parcous de la liste triee pour afficher le resultat
    for(int i = 0; i < N; i++) {
        printf("%d ", tab[i]);
    }
}

void triMinima(int tab[N]) {
    int j,elem;
    // On parcours toute la liste
    for(int i = 0; i < (N - 1); i++) {
        // Pour chaque element, on permute l'element le plus faible pour le ramnener au debut de la boucle en cours
        for(j = i + 1; j < N; j++) {
            if (tab[i] > tab[j]) {
                elem = tab[i];
                tab[i] = tab[j];
                tab[j] = elem;
            }
        }
    }
    // Parcous de la liste triee pour afficher le resultat
    for(int i = 0; i < N; i++) {
        printf("%d ", tab[i]);
    }
}

void triInsertion(int tab[N]) {
    int j,actualValue;
    // On parcours toute la liste a la recherche de la valeur la plus faible
    for (int i = 1 ; i <= (N - 1) ; i++) {
        j = i;
        // Tant que l'element precedent est superieur, on insere la valeur a la place precedente
        while ((j > 0) && (tab[j - 1] > tab[j])) {
            actualValue = tab[j];
            tab[j] = tab[j - 1];
            tab[j - 1] = actualValue;
            j--;
        }
    }
    // Parcous de la liste triee pour afficher le resultat
    for(int i = 0 ; i < N ; i++) {
        printf("%d ", tab[i]);
    }
}

int main(int argc, char** argv) {
    int randomFd;
    int tab[N];
    int typeTri = 1;
    int myRandomInteger;
    size_t randomDataLen = 0;

    printf("\nChoix du tri : \n[1]=>triBulle\n[2]=>triMinima\n[3]=>triInsertion\n\n");
    printf("Tableau avant le tri : ");

    randomFd = open("/dev/random", O_RDONLY);

    // Parcours du tableau pour afficher la liste avant le tri
    if (randomFd < 0) {
        printf("[!] Erreur à l'ouverture de /dev/random");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < N; i++) {
        ssize_t result = read(randomFd, ((char*)&myRandomInteger) + randomDataLen, (sizeof myRandomInteger) - randomDataLen);
        if (result < 0) {
            printf("[!] Erreur à l'ouverture de /dev/random");
            i--;
            continue;
        }
        myRandomInteger = myRandomInteger%50;
        tab[i] = myRandomInteger;
        printf("%d ", tab[i]);
    }
    close(randomFd);
    
    // On verifie que l'on a passe un seul argument au programme
    if(argc == 2) {
        typeTri = atoi(argv[1]);
    }
    // En focntion de la valeur de tri souhaitee, on execute la fonctione associee
    switch (typeTri) {
        case 1:
            printf("\n\nTri Bulle : ");
            triBulle(tab);
            break;

        case 2:
            printf("\n\nTri par recherche des minima successifs : ");
            triMinima(tab);
            break;

        case 3:
            printf("\n\nTri par insertion : ");
            triInsertion(tab);
            printf("\n");
            break;

        default:
            printf("\n\nTri Bulle : ");
            triBulle(tab);
            printf("\n\nTri par recherche des minima successifs : ");
            triMinima(tab);
            printf("\n\nTri par insertion : ");
            triInsertion(tab);
            printf("\n");
    }
    printf("\n");
    return 0;
}
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// Taille du tableau contenant les personnes
#define MAX_SIZE 20

// Possibilités de choix pour le menu
#define CHOICE_CREATE_PERSON 0
#define CHOICE_PRINT_PERSON 1
#define CHOICE_SORT_PERSON 2
#define CHOICE_MODIFY_PERSON 3
#define CHOICE_DELETE_PERSON 4
#define CHOICE_LEAVE 5

// STRUCTURES
struct DATE
{
    int jour;
    int mois;
    int annee;
};

struct PERSONNE
{
    char nom[20];
    char prenom[20];
    struct DATE naissance;
};

// Variables globales
struct PERSONNE *TABLE_OF_PEOPLE[MAX_SIZE];
int nbPersonnes = 0;

// Check buffer
void clear_input_buffer(char* string, int size_of_str)
{
    int length = strlen(string);
    if (length + 1 < size_of_str || string[size_of_str - 2] == '\n')
        string[length - 1] = '\0';
    else
        while (getchar() != '\n');
}

void clean_fgets(char *user_entry, int size_of_str)
{
    if (fgets(user_entry, size_of_str, stdin) == NULL) return;
    clear_input_buffer(user_entry, size_of_str);
}

// Vérifier le nombre de personne
int isEmpty() {
    return nbPersonnes == 0;
}

// Affiche le détail de toutes les personnes dans le tableau
void printPeople(void) {
    struct PERSONNE *it;
    fputs("\n\n     TABLEAU : \n\n",stdout);
    for (int i = 0; i < nbPersonnes; i++) {
        it = TABLE_OF_PEOPLE[i];
        printf("Personne %i : %s %s né(e) le %d/%d/%d\n", i, it->nom, it->prenom, it->naissance.jour, it->naissance.mois, it->naissance.annee);
    }
}

// Fonction de creation de personnes
void createPeople(void) {
    char tmp[50];
    struct PERSONNE *personne = malloc(sizeof(struct PERSONNE));
    fputs("\n   Nom de la personne : ", stdout);
    fflush(stdout);
    clean_fgets(personne->nom, 20);
    fputs("\n   Prénom de la personne : ", stdout);
    fflush(stdout);
    clean_fgets(personne->prenom, 20);
    fputs("\n   Année de naissance : ", stdout);
    fflush(stdout);
    clean_fgets(tmp, 5);
    if (sscanf(tmp, "%d", &personne->naissance.annee) != 1) exit(EXIT_FAILURE);
    fputs("\n   Mois de naissance : ", stdout);
    fflush(stdout);
    clean_fgets(tmp, 3);
    if (sscanf(tmp, "%d", &personne->naissance.mois) != 1) exit(EXIT_FAILURE);
    fputs("\n   Jour de naissance : ", stdout);
    fflush(stdout);
    clean_fgets(tmp, 3);
    if (sscanf(tmp, "%d", &personne->naissance.jour) != 1) exit(EXIT_FAILURE);    
    TABLE_OF_PEOPLE[nbPersonnes] = personne;
    printf("\nPersonne : %s %s né(e) le %d/%d/%d ajoutée au tableau !\n", strtok(personne->nom, "\n"), strtok(personne->prenom, "\n"), personne->naissance.jour, personne->naissance.mois, personne->naissance.annee);
    nbPersonnes++;
    printPeople();
}

// Classer les personnes du tableau par date de naissance
void sortPeople(void) {
    int i = 0;
    while (TABLE_OF_PEOPLE[i + 1] != NULL) {
        // Pour chaque element on compare l'element avec le suivant et les permute si besoin
        for(int j = 0; j <= i; j++) {
            if(TABLE_OF_PEOPLE[j + 1]->naissance.annee < TABLE_OF_PEOPLE[j]->naissance.annee) {
                struct PERSONNE *tmp;              
                tmp = TABLE_OF_PEOPLE[j + 1];
                TABLE_OF_PEOPLE[j + 1] = TABLE_OF_PEOPLE[j];
                TABLE_OF_PEOPLE[j] = tmp;
            }
        }
        i++;
    }
    printPeople();
}

// Retrouver , modifier et afficher une personne via son Nom
void findPeople(void) {
    char tmp[50];
    char *nom_a_chercher = (char *) malloc(sizeof(char) * 20);
    printf("\nQuelle est le nom de la personne que vous voulez modifier?\n");
    printPeople();
    fflush(stdout);
    if (fgets(nom_a_chercher, 20, stdin) == NULL) exit(EXIT_FAILURE);
    if ((!strcmp(nom_a_chercher, "\n")) && (fgets(nom_a_chercher, 20, stdin) == NULL)) exit(EXIT_FAILURE);
    for (int i = 0; i < MAX_SIZE - 1; i++) {
        if (strcmp(TABLE_OF_PEOPLE[i]->nom, nom_a_chercher)) {
            struct PERSONNE *personne = malloc(sizeof(struct PERSONNE));
            fputs("\n   Nom de la personne : ", stdout);
            fflush(stdout);
            clean_fgets(personne->nom, 20);
            fputs("\n   Prénom de la personne : ", stdout);
            fflush(stdout);
            clean_fgets(personne->prenom, 20);
            fputs("\n   Année de naissance : ", stdout);
            fflush(stdout);
            clean_fgets(tmp, 5);
            if (sscanf(tmp, "%d", &personne->naissance.annee) != 1) exit(EXIT_FAILURE);
            fputs("\n   Mois de naissance : ", stdout);
            fflush(stdout);
            clean_fgets(tmp, 3);
            if (sscanf(tmp, "%d", &personne->naissance.mois) != 1) exit(EXIT_FAILURE);
            fputs("\n   Jour de naissance : ", stdout);
            fflush(stdout);
            clean_fgets(tmp, 3);
            if (sscanf(tmp, "%d", &personne->naissance.jour) != 1) exit(EXIT_FAILURE);   
            TABLE_OF_PEOPLE[i] = personne;
            printf("\nPersonne : %s %s né(e) le %d/%d/%d ajoutée au tableau !\n", strtok(personne->nom, "\n"), strtok(personne->prenom, "\n"), personne->naissance.jour, personne->naissance.mois, personne->naissance.annee);
            break;
        }
    } 
    free(nom_a_chercher);
}

// Supprimer une personne du tableau
void deletePeople(void) {
    char tmp[50];
    int choix;
    printf("\nQuelle personne voulez vous supprimer du tableau ? (21 for all)\n");
    printPeople();
    fflush(stdout);
    if (fgets(tmp, 4, stdin) == NULL) exit(EXIT_FAILURE);
    if (sscanf(tmp, "%d", &choix) != 1) exit(EXIT_FAILURE);
    if (choix < 0 || choix > 21) {
        exit(EXIT_FAILURE);
    }
    else {
        if (choix == 21) {
            for (int i = 0; i < MAX_SIZE; i++) {
                if (TABLE_OF_PEOPLE[i] != NULL) {
                    free(TABLE_OF_PEOPLE[i]);
                    TABLE_OF_PEOPLE[i] = NULL;
                    nbPersonnes--;
                }
            }
        }
        else {
            while (TABLE_OF_PEOPLE[choix + 1] != NULL) {
                struct PERSONNE *tmp;              
                tmp = TABLE_OF_PEOPLE[choix + 1];
                TABLE_OF_PEOPLE[choix] = tmp;
                choix++;
            }
        }
    }
    nbPersonnes--;
    printPeople();
}

int main(int argc, char** argv) {
    char dtm[50];
    int choix;

    // Boucle du menu de sélection
    while (choix != CHOICE_LEAVE) {
        if (argc == 2) {
                choix = atoi(argv[1]);
        }
        else {
            // Choix de l'action à réaliser
            printf("\nChoix de l'action : \n[%i]=>Définir une personne\n[%i]=>Afficher les personnes créées\n[%i]=>Classer les noms par date de naissance\n[%i]=>Modifier une personne\n[%i]=>Supprimer une personne\n[%i]=>CHOICE_LEAVE\n\n", CHOICE_CREATE_PERSON, CHOICE_PRINT_PERSON, CHOICE_SORT_PERSON, CHOICE_MODIFY_PERSON, CHOICE_DELETE_PERSON, CHOICE_LEAVE);
            fflush(stdout);
            if (fgets(dtm, 4, stdin) == NULL) exit(EXIT_FAILURE);
            if (sscanf(dtm, "%d", &choix) != 1) exit(EXIT_FAILURE);
            printf("Choix : %i\n", choix);
        }
        switch (choix) {
            case CHOICE_CREATE_PERSON:
                // Vérification de l'espace disponible dans le tableau 
                if(nbPersonnes < MAX_SIZE) {
                    createPeople();
                }
                else {
                    printf("Tableau plein !!");
                }
                break;

            case CHOICE_PRINT_PERSON:
                if (isEmpty()) {
                    fputs("\n\n-------------------------\n  [!] Tableau vide !!!\n-------------------------\n\n",stdout);
                    break;
                } else { 
                    printPeople();
                    break;
                }

            case CHOICE_SORT_PERSON:
                if (isEmpty()) {
                    fputs("\n\n-------------------------\n  [!] Tableau vide !!!\n-------------------------\n\n",stdout);
                    break;
                } else { 
                    sortPeople();
                    break;
                }
            
            case CHOICE_MODIFY_PERSON:
                if (isEmpty()) {
                    fputs("\n\n---------------------------------------------------------------------------\n  [!] Tableau vide : veuillez créer une personne pour la modifier !!!\n---------------------------------------------------------------------------\n\n",stdout);
                    break;
                } else { 
                    findPeople();
                    break;
                }
            
            case CHOICE_DELETE_PERSON:
                if (isEmpty()) {
                    fputs("\n\n------------------------------\n  [!] Tableau déjà vide !!!\n------------------------------\n\n",stdout);
                    break;
                } else { 
                    deletePeople();
                    break;
                }

            case CHOICE_LEAVE:
                break;

            default:
                return EXIT_FAILURE;
        }
    }
    printf("A BIENTOT !!!\n");
    return EXIT_SUCCESS;
}

# Root makefile

DIR = $(shell ls -d TP*/ --format=single-column)

# Redirect the target to each makefile
%:
	for f in $(DIR) ; do $(MAKE) $@ -C $$f ; done

# The target is empty only to enable "make" to work
all:

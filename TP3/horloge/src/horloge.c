#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#define CONVERT_SUCCESS 0
#define CONVERT_NULL_POINTER 1
#define CONVERT_INVALID_HOUR 2
#define CONVERT_INVALID_MINUTES 3
#define CONVERT_INVALID_SECONDS 4
#define CONVERT_OVERFLOW 5

char* convertVerbose(int code) {
    switch (code) {
        case CONVERT_SUCCESS:
            return "Everything worked fine.";
        case CONVERT_NULL_POINTER:
            return "The given result pointer (int*) cannot be NULL.";
        case CONVERT_INVALID_HOUR:
            return "Converting negative hours is not allowed.";
        case CONVERT_INVALID_MINUTES:
            return "Converting negative minutes is not allowed, or the given minutes are more than 60: use an hour.";
        case CONVERT_INVALID_SECONDS:
            return "Converting negative seconds is not allowed, or the given seconds are more than 60: use a minute.";
        case CONVERT_OVERFLOW:
            return "The given values cannot be converted because of an overflow.";
        default:
            return "THIS ERROR CODE IS NOT HANDLED";
    }
}

int convert(int* res, int hours, int minutes, int seconds) {
    if (res == NULL) {
        return CONVERT_NULL_POINTER;
    }

    // Validate hours
    if (hours < 0) {
        return CONVERT_INVALID_HOUR;
    }

    // Validate minutes
    if (minutes < 0 || minutes >= 60) {
        return CONVERT_INVALID_MINUTES;
    }

    // Validate seconds
    if (seconds < 0 || seconds >= 60) {
        return CONVERT_INVALID_SECONDS;
    }

    // Add seconds to result
    *res = seconds;

    // Add minutes to result
    if (minutes > INT_MAX / 60) {
        return CONVERT_OVERFLOW;
    }
    int minInSec = minutes * 60;
    if (*res > INT_MAX - minInSec) {
        return CONVERT_OVERFLOW;
    }
    *res += minInSec;

    // Add hours to result
    if (hours > INT_MAX / (60 * 60)) {
        return CONVERT_OVERFLOW;
    }
    int hoursInSec = hours * (60 * 60);
    if (*res > INT_MAX - hoursInSec) {
        return CONVERT_OVERFLOW;
    }
    *res += hoursInSec;

    return CONVERT_SUCCESS;
}

int main(int argc, char** argv) {
    if (argc != 4) {
        printf("Did not find the expected args as [bin hours minutes seconds] in CLI.\n");
        return 1;
    }

    int h = atoi(argv[1]);
    int m = atoi(argv[2]);
    int s = atoi(argv[3]);

    int res, err;
    err = convert(&res, h, m, s);

    if (err != CONVERT_SUCCESS) {
        printf("Convert failed with %d:%d:%d. %s\n", h, m, s, convertVerbose(err));
        return 1;
    }

    printf("%d:%d:%d = %d sec.\n", h, m, s, res);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "degre.h"

#define NO_CHOICE 0
#define CTOK 1
#define CTOF 2
#define KTOC 3
#define KTOF 4
#define FTOC 5
#define FTOK 6


int lireChoix() {
    int choice = -1;
    while (choice == -1) {
        printf( "--- Conversions --\nDISCLAIMER: because of the unprecise arithmetic, those results could not be exact, and the conversion should not work\n1. STOP\n2. °C -> °F\n3. °C -> °K\n4. °K -> °C\n5. °K -> °F\n6. °F -> °C\n7. °F -> °K\n\nYour choice: ");
        char ch = getchar();
        printf("\n\n");

        switch (ch) {
            case '0':
                choice = NO_CHOICE;
                break;
            case '1':
                choice = CTOF;
                break;
            case '2':
                choice = CTOK;
                break;
            case '3':
                choice = KTOC;
                break;
            case '4':
                choice = KTOF;
                break;
            case '5':
                choice = FTOC;
                break;
            case '6':
                choice = FTOK;
                break;
        }
    }
    return choice;
}

typedef int (*ConvertFunc)(float* res, float degre);

void executerChoix(int choix) {
    ConvertFunc cf;
    switch (choix) {
        case NO_CHOICE:
            return;
        case CTOF:
            cf = &CelsiusAFahrenheit;
            break;
        case CTOK:
            cf = &CelsiusAKelvin;
            break;
        case KTOC:
            cf = &KelvinACelsius;
            break;
        case KTOF:
            cf = &KelvinAFahrenheit;
            break;
        case FTOC:
            cf = &FahrenheitACelsius;
            break;
        case FTOK:
            cf = &FahrenheitAKelvin;
            break;
        default:
            printf("Unrecognized choice [%d]\n", choix);
            return;
    }

    printf("Value to convert: \n");
    char buffer[16];
    int err = read(STDIN_FILENO, buffer, 16);
    printf("\n");
    if (err == -1) {
        printf("An error occured while reading.\n");
    }

    float res, degre = atof(buffer);
    err = cf(&res, degre);
    if (err != DEGRE_SUCCESS) {
        printf("Failed to convert. %s\n", degreVerbose(err));
        return;
    }

    printf("Result = %f.\n", res);
}

int main() {
    int choice;
    do {
        choice = lireChoix();
        executerChoix(choice);
    } while (choice != NO_CHOICE);

    return 0;
}

// Those tests should have been more efficient,
// but for the fun they are more complex than it
// could be.
// They match the Go tests stdout structure,
// without the benefits of it... And it's hardcoded
// so a big work should be done to really match
// the advantages of Go tests. (But I won't dev a whole
// framework just for 6 UT).

#include <limits.h>
#include <stdio.h>
#include <stddef.h>

#include "degre.h"

#define SUCCESS 0
#define FAILURE 1
#define length(t) sizeof(t) / sizeof((t)[0])

#ifdef __GOLIKE_TESTS__
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define run(align, str) for (int i = 0; i < align; i++) { printf("    "); } printf("=== RUN   %s\n", str);
#define pass(align, str) for (int i = 0; i < align; i++) { printf("    "); } printf("--- PASS: %s\n", str);
#define fail(align, str) for (int i = 0; i < align; i++) { printf("    "); } printf("--- FAIL: %s\n", str);
#endif

typedef struct {
    int  status;
    char* name;
} TestResult;

typedef struct {
    char*  name;
    float* resPtr;
    float  degre;
    int    expectedErr;
    float  expectedRes;
} TestConversion;

TestResult TestCelsiusAKelvin() {
#ifdef __GOLIKE_TESTS__
    run(0, __func__);
#endif

    float validRes;
    TestConversion tests[] = {
        {
            .name        = "zero-celsius",
            .resPtr      = &validRes,
            .degre       = 0.f,
            .expectedErr = DEGRE_SUCCESS,
            .expectedRes = 273.15f,
        }, {
            .name        = "zero-kelvin",
            .resPtr      = &validRes,
            .degre       = -273.15f,
            .expectedErr = DEGRE_SUCCESS,
            .expectedRes = 0.f,
        }, {
            .name        = "out-of-bounds",
            .resPtr      = &validRes,
            .degre       = LONG_MIN,
            .expectedErr = DEGRE_OUT_OF_BOUNDS,
            .expectedRes = 0.f,
        }, {
            .name        = "res-null",
            .resPtr      = NULL,
            .degre       = 0.f,
            .expectedErr = DEGRE_RES_NULL,
            .expectedRes = 0.f,
        },
        // This UT can't work because of float arithmetic
        //{
        //    "overflow",
        //    &validRes,
        //    LONG_MAX,
        //    DEGRE_OUT_OF_BOUNDS,
        //    0.f,
        //}
    };

#ifdef __GOLIKE_TESTS__
    char* testname;
    time_t start_t, end_t;
#endif

    TestResult tr = {SUCCESS, ""};
    int len = length(tests);
    int i;
    for (i = 0; i < len; i++) {
#ifdef __GOLIKE_TESTS__
        int testnameLen = strlen(__func__) + 1 + strlen(tests[i].name);
        testname = (char*)malloc(sizeof(char) * testnameLen + 1);
        sprintf(testname, "%s/%s", __func__, tests[i].name);
        run(1, testname);
        time(&start_t);
#endif

        // UT payload
        int err = CelsiusAKelvin(tests[i].resPtr, tests[i].degre);

        if (err != tests[i].expectedErr) {
            printf("Expected error: %d; Got: %d.\n", tests[i].expectedErr, err);

            tr.status = FAILURE;
            tr.name = tests[i].name;
            break;
        }

        if (tests[i].resPtr != NULL) {
            if (*tests[i].resPtr != tests[i].expectedRes) {
                printf("Expected result: %f; Got: %f.\n", tests[i].expectedRes, *tests[i].resPtr);

                tr.status = FAILURE;
                tr.name = tests[i].name;
                break;
            }
            // Reset pointer value
            *tests[i].resPtr = 0.f;
        }

#ifdef __GOLIKE_TESTS__
        time(&end_t);
        double diff_t = difftime(end_t, start_t);
        char* end_testname = malloc(sizeof(char) * (strlen(testname) + 16));
        sprintf(end_testname, "%s (%.3fs)", testname, diff_t);
        pass(1, end_testname);
        free(end_testname);
#endif
    }

#ifdef __GOLIKE_TESTS__
    switch (tr.status) {
        case SUCCESS:
            pass(0, __func__);
            break;
        case FAILURE:
        default:
            fail(1, testname);
            fail(0, __func__);
    }
    free(testname);
#endif
    return tr;
}

TestResult TestCelsiusAFahrenheit() {
#ifdef __GOLIKE_TESTS__
    run(0, __func__);
#endif

    float validRes;
    TestConversion tests[] = {
        {
            .name        = "zero-celsius",
            .resPtr      = &validRes,
            .degre       = 0.f,
            .expectedErr = DEGRE_SUCCESS,
            .expectedRes = 32.f,
        },
        // This UT can't work because of float arithmetic
        //{
        //    .name        = "zero-fahrenheit",
        //    .resPtr      = &validRes,
        //    .degre       = -17.7778f,
        //    .expectedErr = DEGRE_SUCCESS,
        //    .expectedRes = 0.f,
        //},
        {
            .name        = "out-of-bounds",
            .resPtr      = &validRes,
            .degre       = LONG_MIN,
            .expectedErr = DEGRE_OUT_OF_BOUNDS,
            .expectedRes = 0.f,
        }, {
            .name        = "res-null",
            .resPtr      = NULL,
            .degre       = 0.f,
            .expectedErr = DEGRE_RES_NULL,
            .expectedRes = 0.f,
        }, {
            .name        = "overflow",
            .resPtr      = &validRes,
            .degre       = LONG_MAX,
            .expectedErr = DEGRE_OVERFLOW,
            .expectedRes = 0.f,
        }
    };

#ifdef __GOLIKE_TESTS__
    char* testname;
    time_t start_t, end_t;
#endif

    TestResult tr = {SUCCESS, ""};
    int len = length(tests);
    int i;
    for (i = 0; i < len; i++) {
#ifdef __GOLIKE_TESTS__
        int testnameLen = strlen(__func__) + 1 + strlen(tests[i].name);
        testname = (char*)malloc(sizeof(char) * testnameLen + 1);
        sprintf(testname, "%s/%s", __func__, tests[i].name);
        run(1, testname);
        time(&start_t);
#endif

        // UT payload
        int err = CelsiusAFahrenheit(tests[i].resPtr, tests[i].degre);

        if (err != tests[i].expectedErr) {
            printf("Expected error: %d; Got: %d.\n", tests[i].expectedErr, err);

            tr.status = FAILURE;
            tr.name = tests[i].name;
            break;
        }

        if (tests[i].resPtr != NULL) {
            if (*tests[i].resPtr != tests[i].expectedRes) {
                printf("Expected result: %f; Got: %f.\n", tests[i].expectedRes, *tests[i].resPtr);

                tr.status = FAILURE;
                tr.name = tests[i].name;
                break;
            }
            // Reset pointer value
            *tests[i].resPtr = 0.f;
        }

#ifdef __GOLIKE_TESTS__
        time(&end_t);
        double diff_t = difftime(end_t, start_t);
        char* end_testname = malloc(sizeof(char) * (strlen(testname) + 16));
        sprintf(end_testname, "%s (%.3fs)", testname, diff_t);
        pass(1, end_testname);
        free(end_testname);
#endif
    }

#ifdef __GOLIKE_TESTS__
    switch (tr.status) {
        case SUCCESS:
            pass(0, __func__);
            break;
        case FAILURE:
        default:
            fail(1, testname);
            fail(0, __func__);
    }
    free(testname);
#endif
    return tr;
}

TestResult TestKelvinACelsius() {
#ifdef __GOLIKE_TESTS__
    run(0, __func__);
#endif

    float validRes;
    TestConversion tests[] = {
        {
            .name        = "zero-kelvin",
            .resPtr      = &validRes,
            .degre       = 0.f,
            .expectedErr = DEGRE_SUCCESS,
            .expectedRes = -273.15f,
        }, {
            .name        = "zero-celsius",
            .resPtr      = &validRes,
            .degre       = 273.15f,
            .expectedErr = DEGRE_SUCCESS,
            .expectedRes = 0.f,
        }, {
            .name        = "out-of-bounds",
            .resPtr      = &validRes,
            .degre       = LONG_MIN,
            .expectedErr = DEGRE_OUT_OF_BOUNDS,
            .expectedRes = 0.f,
        }, {
            .name        = "res-null",
            .resPtr      = NULL,
            .degre       = 0.f,
            .expectedErr = DEGRE_RES_NULL,
            .expectedRes = 0.f,
        },
    };

#ifdef __GOLIKE_TESTS__
    char* testname;
    time_t start_t, end_t;
#endif

    TestResult tr = {SUCCESS, ""};
    int len = length(tests);
    int i;
    for (i = 0; i < len; i++) {
#ifdef __GOLIKE_TESTS__
        int testnameLen = strlen(__func__) + 1 + strlen(tests[i].name);
        testname = (char*)malloc(sizeof(char) * testnameLen + 1);
        sprintf(testname, "%s/%s", __func__, tests[i].name);
        run(1, testname);
        time(&start_t);
#endif

        // UT payload
        int err = KelvinACelsius(tests[i].resPtr, tests[i].degre);

        if (err != tests[i].expectedErr) {
            printf("Expected error: %d; Got: %d.\n", tests[i].expectedErr, err);

            tr.status = FAILURE;
            tr.name = tests[i].name;
            break;
        }

        if (tests[i].resPtr != NULL) {
            if (*tests[i].resPtr != tests[i].expectedRes) {
                printf("Expected result: %f; Got: %f.\n", tests[i].expectedRes, *tests[i].resPtr);

                tr.status = FAILURE;
                tr.name = tests[i].name;
                break;
            }
            // Reset pointer value
            *tests[i].resPtr = 0.f;
        }

#ifdef __GOLIKE_TESTS__
        time(&end_t);
        double diff_t = difftime(end_t, start_t);
        char* end_testname = malloc(sizeof(char) * (strlen(testname) + 16));
        sprintf(end_testname, "%s (%.3fs)", testname, diff_t);
        pass(1, end_testname);
        free(end_testname);
#endif
    }

#ifdef __GOLIKE_TESTS__
    switch (tr.status) {
        case SUCCESS:
            pass(0, __func__);
            break;
        case FAILURE:
        default:
            fail(1, testname);
            fail(0, __func__);
    }
    free(testname);
#endif
    return tr;
}

TestResult TestKelvinAFahrenheit() {
#ifdef __GOLIKE_TESTS__
    run(0, __func__);
#endif

    float validRes;
    TestConversion tests[] = {
        {
            .name        = "zero-kelvin",
            .resPtr      = &validRes,
            .degre       = 0.f,
            .expectedErr = DEGRE_SUCCESS,
            .expectedRes = -459.669983f,
        },
        // This UT can't work because of float arithmetic
        //{
        //    .name        = "zero-fahrenheit",
        //    .resPtr      = &validRes
        //    .degre       = 255.372f,
        //    .expectedErr = DEGRE_SUCCESS,
        //    .expectedRes = -0.000401f,
        //},
        {
            .name        = "out-of-bounds",
            .resPtr      = &validRes,
            .degre       = LONG_MIN,
            .expectedErr = DEGRE_OUT_OF_BOUNDS,
            .expectedRes = 0.f,
        }, {
            .name        = "overflow",
            .resPtr      = &validRes,
            .degre       = LONG_MAX,
            .expectedErr = DEGRE_OVERFLOW,
            .expectedRes = 0.f,
        }, {
            .name        = "res-null",
            .resPtr      = NULL,
            .degre       = 0.f,
            .expectedErr = DEGRE_RES_NULL,
            .expectedRes = 0.f,
        },
    };

#ifdef __GOLIKE_TESTS__
    char* testname;
    time_t start_t, end_t;
#endif

    TestResult tr = {SUCCESS, ""};
    int len = length(tests);
    int i;
    for (i = 0; i < len; i++) {
#ifdef __GOLIKE_TESTS__
        int testnameLen = strlen(__func__) + 1 + strlen(tests[i].name);
        testname = (char*)malloc(sizeof(char) * testnameLen + 1);
        sprintf(testname, "%s/%s", __func__, tests[i].name);
        run(1, testname);
        time(&start_t);
#endif

        // UT payload
        int err = KelvinAFahrenheit(tests[i].resPtr, tests[i].degre);

        if (err != tests[i].expectedErr) {
            printf("Expected error: %d; Got: %d.\n", tests[i].expectedErr, err);

            tr.status = FAILURE;
            tr.name = tests[i].name;
            break;
        }

        if (tests[i].resPtr != NULL) {
            if (*tests[i].resPtr != tests[i].expectedRes) {
                printf("Expected result: %f; Got: %f.\n", tests[i].expectedRes, *tests[i].resPtr);

                tr.status = FAILURE;
                tr.name = tests[i].name;
                break;
            }
            // Reset pointer value
            *tests[i].resPtr = 0.f;
        }

#ifdef __GOLIKE_TESTS__
        time(&end_t);
        double diff_t = difftime(end_t, start_t);
        char* end_testname = malloc(sizeof(char) * (strlen(testname) + 16));
        sprintf(end_testname, "%s (%.3fs)", testname, diff_t);
        pass(1, end_testname);
        free(end_testname);
#endif
    }

#ifdef __GOLIKE_TESTS__
    switch (tr.status) {
        case SUCCESS:
            pass(0, __func__);
            break;
        case FAILURE:
        default:
            fail(1, testname);
            fail(0, __func__);
    }
    free(testname);
#endif
    return tr;
}

TestResult TestFahrenheitACelsius() {
#ifdef __GOLIKE_TESTS__
    run(0, __func__);
#endif

    float validRes;
    TestConversion tests[] = {
        {
            .name        = "zero-fahrenheit",
            .resPtr      = &validRes,
            .degre       = 0.f,
            .expectedErr = DEGRE_SUCCESS,
            .expectedRes = -17.777779f,
        }, {
           .name        = "zero-celsius",
           .resPtr      = &validRes,
           .degre       = 32.f,
           .expectedErr = DEGRE_SUCCESS,
           .expectedRes = 0.f,
        }, {
            .name        = "out-of-bounds",
            .resPtr      = &validRes,
            .degre       = LONG_MIN,
            .expectedErr = DEGRE_OUT_OF_BOUNDS,
            .expectedRes = 0.f,
        }, {
            .name        = "res-null",
            .resPtr      = NULL,
            .degre       = 0.f,
            .expectedErr = DEGRE_RES_NULL,
            .expectedRes = 0.f,
        },
    };

#ifdef __GOLIKE_TESTS__
    char* testname;
    time_t start_t, end_t;
#endif

    TestResult tr = {SUCCESS, ""};
    int len = length(tests);
    int i;
    for (i = 0; i < len; i++) {
#ifdef __GOLIKE_TESTS__
        int testnameLen = strlen(__func__) + 1 + strlen(tests[i].name);
        testname = (char*)malloc(sizeof(char) * testnameLen + 1);
        sprintf(testname, "%s/%s", __func__, tests[i].name);
        run(1, testname);
        time(&start_t);
#endif

        // UT payload
        int err = FahrenheitACelsius(tests[i].resPtr, tests[i].degre);

        if (err != tests[i].expectedErr) {
            printf("Expected error: %d; Got: %d.\n", tests[i].expectedErr, err);

            tr.status = FAILURE;
            tr.name = tests[i].name;
            break;
        }

        if (tests[i].resPtr != NULL) {
            if (*tests[i].resPtr != tests[i].expectedRes) {
                printf("Expected result: %f; Got: %f.\n", tests[i].expectedRes, *tests[i].resPtr);

                tr.status = FAILURE;
                tr.name = tests[i].name;
                break;
            }
            // Reset pointer value
            *tests[i].resPtr = 0.f;
        }

#ifdef __GOLIKE_TESTS__
        time(&end_t);
        double diff_t = difftime(end_t, start_t);
        char* end_testname = malloc(sizeof(char) * (strlen(testname) + 16));
        sprintf(end_testname, "%s (%.3fs)", testname, diff_t);
        pass(1, end_testname);
        free(end_testname);
#endif
    }

#ifdef __GOLIKE_TESTS__
    switch (tr.status) {
        case SUCCESS:
            pass(0, __func__);
            break;
        case FAILURE:
        default:
            fail(1, testname);
            fail(0, __func__);
    }
    free(testname);
#endif
    return tr;
}

TestResult TestFahrenheitAKelvin() {
#ifdef __GOLIKE_TESTS__
    run(0, __func__);
#endif

    float validRes;
    TestConversion tests[] = {
        {
            .name        = "zero-fahrenheit",
            .resPtr      = &validRes,
            .degre       = 0.f,
            .expectedErr = DEGRE_SUCCESS,
            .expectedRes = 255.372223f,
        }, {
           .name        = "zero-kelvin",
           .resPtr      = &validRes,
           .degre       = -459.67f,
           .expectedErr = DEGRE_SUCCESS,
           .expectedRes = 0.f,
        }, {
            .name        = "out-of-bounds",
            .resPtr      = &validRes,
            .degre       = LONG_MIN,
            .expectedErr = DEGRE_OUT_OF_BOUNDS,
            .expectedRes = 0.f,
        }, {
            .name        = "res-null",
            .resPtr      = NULL,
            .degre       = 0.f,
            .expectedErr = DEGRE_RES_NULL,
            .expectedRes = 0.f,
        },
    };

#ifdef __GOLIKE_TESTS__
    char* testname;
    time_t start_t, end_t;
#endif

    TestResult tr = {SUCCESS, ""};
    int len = length(tests);
    int i;
    for (i = 0; i < len; i++) {
#ifdef __GOLIKE_TESTS__
        int testnameLen = strlen(__func__) + 1 + strlen(tests[i].name);
        testname = (char*)malloc(sizeof(char) * testnameLen + 1);
        sprintf(testname, "%s/%s", __func__, tests[i].name);
        run(1, testname);
        time(&start_t);
#endif

        // UT payload
        int err = FahrenheitAKelvin(tests[i].resPtr, tests[i].degre);

        if (err != tests[i].expectedErr) {
            printf("Expected error: %d; Got: %d.\n", tests[i].expectedErr, err);

            tr.status = FAILURE;
            tr.name = tests[i].name;
            break;
        }

        if (tests[i].resPtr != NULL) {
            if (*tests[i].resPtr != tests[i].expectedRes) {
                printf("Expected result: %f; Got: %f.\n", tests[i].expectedRes, *tests[i].resPtr);

                tr.status = FAILURE;
                tr.name = tests[i].name;
                break;
            }
            // Reset pointer value
            *tests[i].resPtr = 0.f;
        }

#ifdef __GOLIKE_TESTS__
        time(&end_t);
        double diff_t = difftime(end_t, start_t);
        char* end_testname = malloc(sizeof(char) * (strlen(testname) + 16));
        sprintf(end_testname, "%s (%.3fs)", testname, diff_t);
        pass(1, end_testname);
        free(end_testname);
#endif
    }

#ifdef __GOLIKE_TESTS__
    switch (tr.status) {
        case SUCCESS:
            pass(0, __func__);
            break;
        case FAILURE:
        default:
            fail(1, testname);
            fail(0, __func__);
    }
    free(testname);
#endif
    return tr;
}

int main() {
    printf("RUN\n");

    TestResult tr;

    // Test °C -> X
    tr = TestCelsiusAKelvin();
    if (tr.status != SUCCESS) {
        return 1;
    }

    tr = TestCelsiusAFahrenheit();
    if (tr.status != SUCCESS) {
        return 1;
    }

    // Test °K -> X
    tr = TestKelvinACelsius();
    if (tr.status != SUCCESS) {
        return 1;
    }

    tr = TestKelvinAFahrenheit();
    if (tr.status != SUCCESS) {
        return 1;
    }

    // Test °F -> X
    tr = TestFahrenheitACelsius();
    if (tr.status != SUCCESS) {
        return 1;
    }

    tr = TestFahrenheitAKelvin();
    if (tr.status != SUCCESS) {
        return 1;
    }

    printf("PASS\n");

    return 0;
}

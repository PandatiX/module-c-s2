const char* degreVerbose(int code);

// DISCLAIMER: those functions does not support
// negative thermodynamic temperatures.

#define DEGRE_SUCCESS 0
#define DEGRE_RES_NULL 1
#define DEGRE_OUT_OF_BOUNDS 2
#define DEGRE_OVERFLOW 3

int CelsiusAKelvin(float* res, float degre);
int CelsiusAFahrenheit(float* res, float degre);

int KelvinACelsius(float* res, float degre);
int KelvinAFahrenheit(float* res, float degre);

int FahrenheitACelsius(float* res, float degre);
int FahrenheitAKelvin(float* res, float degre);

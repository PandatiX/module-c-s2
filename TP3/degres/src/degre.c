#include <limits.h>
#include <stddef.h>
#include <stdio.h>

#include "degre.h"

const char* degreVerbose(int code) {
    switch (code) {
        case DEGRE_SUCCESS:
            return "Everything worked fine.";
        case DEGRE_RES_NULL:
            return "The provided res pointer (float*) can not be NULL.";
        case DEGRE_OUT_OF_BOUNDS:
            return "The provided temperature is out of its bounds according to positive thermodynamic temperatures.";
        case DEGRE_OVERFLOW:
            return "The provided temperature can not be converted because of an overflow.";
        default:
            return "THIS ERROR CODE IS NOT HANDLED";
    }
}

#define testNull(r) if (r == NULL) { return DEGRE_RES_NULL; }

// Those macros ensure bounds for positive thermodynamic temperatures
#define checkCelsiusBounds(n) if (n < -273.15f) { return DEGRE_OUT_OF_BOUNDS; }
#define checkKelvinBounds(n) if (n < 0) { return DEGRE_OUT_OF_BOUNDS; }
#define checkFahrenheitBounds(n) if (n < -459.67f) { return DEGRE_OUT_OF_BOUNDS; }

int CelsiusAKelvin(float* res, float degre) {
    testNull(res);

    checkCelsiusBounds(degre);

    // Check overflow
    if (degre > LONG_MAX - 273.15f) {
        return DEGRE_OVERFLOW;
    }

    // No underflow possible there

    *res = degre + 273.15f;
    return DEGRE_SUCCESS;
}

int CelsiusAFahrenheit(float* res, float degre) {
    testNull(res);

    checkCelsiusBounds(degre);

    // Check overflow
    if (degre > LONG_MAX / 9.f * 5.f - 32.f) {
        return DEGRE_OVERFLOW;
    }

    // No underflow possible there

    *res = degre / 5.f * 9.f + 32.f;
    return DEGRE_SUCCESS;
}

int KelvinACelsius(float* res, float degre) {
    testNull(res);

    checkKelvinBounds(degre);

    // No over/underflow possible there

    *res = degre - 273.15f;
    return DEGRE_SUCCESS;
}

int KelvinAFahrenheit(float* res, float degre) {
    testNull(res);

    checkKelvinBounds(degre);

    // Check overflow
    if (degre > (LONG_MAX - 32.f) / 9.f * 5.f + 273.15f) {
        return DEGRE_OVERFLOW;
    }

    // No underflow possible there

    *res = (degre - 273.15f) / 5.f * 9.f + 32.f;
    return DEGRE_SUCCESS;
}

int FahrenheitACelsius(float* res, float degre) {
    testNull(res);

    checkFahrenheitBounds(degre);

    // No over/underflow possible there

    *res = (degre - 32.f) / 9.f * 5.f;
    return DEGRE_SUCCESS;
}

int FahrenheitAKelvin(float* res, float degre) {
    testNull(res);

    checkFahrenheitBounds(degre);

    // No over/underflow possible there

    *res = (degre - 32.f) / 9.f * 5.f + 273.15f;
    return DEGRE_SUCCESS;
}

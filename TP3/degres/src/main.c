#include <stdio.h>
#include <limits.h>

#include "degre.h"

int main() {
    float res; int err;
    err = CelsiusAFahrenheit(&res, 0);
    CelsiusAKelvin(&res, 0);
    KelvinACelsius(&res, 0);
    KelvinAFahrenheit(&res, 0);
    FahrenheitACelsius(&res, 0);
    printf("%f\n", res);
    FahrenheitAKelvin(&res, 0);

    printf("%f - %d\n", res, err);
}

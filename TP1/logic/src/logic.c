#include <stdio.h>

void lg1() {
    int x = 5 & 6;
    /*
     5   = 0b101
     6   = 0b110
     5&6 = 0b100 = 4
     */
    printf("%x\n", x);
}

void lg2() {
    int x = 5 && 6;
    /*
     5    = 0b101
     6    = 0b110
     5&&6 = 0b001 while  5 is "true" and 6 is "true" (different of 0)
     */
    printf("%x\n", x);
}

void lg3() {
    int x = 5 | 6;
    /*
     5   = 0b101
     6   = 0b110
     5|6 = 0b111 = 7
     */
    printf("%x\n", x);
}

void lg4() {
    int x = 5 || 6;
    /*
     5    = 0b101
     6    = 0b110
     5||6 = 0b001 while 5 is "true" (different of 0)
     */
    printf("%x\n", x);
}

void lg5() {
    int x = 5 ^ 6;
    /*
     5   = 0b101
     6   = 0b110
     5^6 = 0b011 = 3
     */
    printf("%x\n", x);
}

void lg6() {
    //int x = 5 ^^ 6;           // Can't compile: ^^ is not a valid C operator... do you want a pow ?
    //printf("%x\n", x);
}

void lg7() {
    int x = ~5;
    /*
     5  = 0b0000000000000101
     ~5 = 0b1111111111111010 = 0xfffffffa
     */
    printf("%x\n", x);
}

void lg8() {
    int x = !5;
    /*
     5  = 0b101
     !5 = 0x000 while 5 is "true" (different of 0), so NOT "true" is "false"
     */
    printf("%x\n", x);
}

int main() {
    lg1();
    lg2();
    lg3();
    lg4();
    lg5();
    lg6();
    lg7();
    lg8();

    return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define BUFFER_SIZE 11

#define MAX_INT_TO_SUM 65535

#define SUM_SUCCESS 0
#define SUM_FAILURE 1
#define SUM_INVALID_VALUE 2
#define SUM_OVERFLOW 3

char* intToStr(int a) {
    char* buffer = (char*)malloc(BUFFER_SIZE);
    sprintf(buffer, "%d", a);
    return buffer;
}

char* verboseSum(int code) {
    switch(code) {
        case SUM_SUCCESS:
            return "Everything worked fine.";
        case SUM_FAILURE:
            return "Something bad happened.";
        case SUM_INVALID_VALUE:
            return "The given value is out of bounds.";
        case SUM_OVERFLOW:
            return "The operation is not possible because of an overflow.";
        default:
            return "THIS ERROR CODE IS NOT HANDLED";
    }
}

int invalidSum(int a) {
    return (a < 0 || a > MAX_INT_TO_SUM);
}

int sumForInc(int* res, int toSum) {
    if (res == NULL) {
        return SUM_FAILURE;
    }

    if (invalidSum(toSum)) {
        return SUM_INVALID_VALUE;
    }

    *res = 0;
    for (int i = 1; i <= toSum; i++) {
        if (*res > INT_MAX - i) {
            return SUM_OVERFLOW;
        }
        *res += i;
    }

    return SUM_SUCCESS;
}

int sumForDec(int* res, int toSum) {
    if (res == NULL) {
        return SUM_FAILURE;
    }

    if (invalidSum(toSum)) {
        return SUM_INVALID_VALUE;
    }

    *res = 0;
    for (int i = toSum; i > 0; i--) {
        if (*res > INT_MAX - i) {
            return SUM_OVERFLOW;
        }
        *res += i;
    }

    return SUM_SUCCESS;
}

int sumWhileInc(int* res, int toSum) {
    if (res == NULL) {
        return SUM_FAILURE;
    }

    if (invalidSum(toSum)) {
        return SUM_INVALID_VALUE;
    }

    *res = 0;
    int i = 1;
    while(i <= toSum) {
        if (*res > INT_MAX - i) {
            return SUM_OVERFLOW;
        }
        *res += i++;
    }

    return SUM_SUCCESS;
}

int sumWhileDec(int* res, int toSum) {
    if (res == NULL) {
        return SUM_FAILURE;
    }

    if (invalidSum(toSum)) {

        return SUM_INVALID_VALUE;
    }

    *res = 0;
    int i = toSum;
    while(i > 0) {
        if (*res > INT_MAX - i) {
            return SUM_OVERFLOW;
        }
        *res += i--;
    }

    return SUM_SUCCESS;
}

void printResult(char* key, int res) {
    printf("[%s] result = %d.\n", key, res);
}

int main(int argc, char** argv) {
    if (argc != 2) {
        return 1;
    }

    int n = atoi(argv[1]);

    int resFI, resFD, resWI, resWD;
    int errFI, errFD, errWI, errWD;
    errFI = sumForInc(&resFI, n);
    errFD = sumForDec(&resFD, n);
    errWI = sumWhileInc(&resWI, n);
    errWD = sumWhileDec(&resWD, n);

    int sumErr = errFI | errFD | errWI | errWD;
    if (sumErr) {
        printf("Failed to sum %d. %s\n", n, verboseSum(sumErr));
        return 1;
    }

    printResult("For Inc", resFI);
    printResult("For Dec", resFD);
    printResult("While Inc", resWI);
    printResult("While Dec", resWD);

    return 0;
}


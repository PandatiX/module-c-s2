#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#define DIV '/'
#define MUL '*'
#define MIN '-'
#define SUM '+'

#define STOP_CHAR 's'

#define COMPUTE_SUCCESS 0
#define COMPUTE_UNDEFINED_OPERATOR 1
#define COMPUTE_IMPOSSIBLE_OPERATION 2

#define MAIN_SUCCESS 0
#define MAIN_NO_STOP_CHAR 1

const char* computeVerbose(int code) {
    switch (code) {
        case COMPUTE_SUCCESS:
            return "Everything worked fine.";
        case COMPUTE_UNDEFINED_OPERATOR:
            return "The specified operator is not defined.";
        case COMPUTE_IMPOSSIBLE_OPERATION:
            return "The operation is not possible (e.g. div by 0).";
        default:
            return "THIS ERROR CODE IS NOT HANDLED";
    }
}

// TODO check overflow
int compute1(float *res, char op, float a, float b) {
    if (op == DIV) {
        if (b == 0) {
            return COMPUTE_IMPOSSIBLE_OPERATION;
        }
        *res = a / b;
    } else if (op == MUL) {
        *res = a * b;
    } else if (op == MIN) {
        *res = a - b;
    } else if (op == SUM) {
        *res = a + b;
    } else {
        return COMPUTE_UNDEFINED_OPERATOR;
    }
    return COMPUTE_SUCCESS;
}

int compute2(float *res, char op, float a, float b) {
    switch (op) {
        case DIV:
            if (b == 0) {
                return COMPUTE_IMPOSSIBLE_OPERATION;
            }
            *res = a / b;
            break;
        case MUL:
            *res = a * b;
            break;
        case MIN:
            *res = a - b;
            break;
        case SUM:
            *res = a + b;
            break;
        default:
            return COMPUTE_UNDEFINED_OPERATOR;
    }
    return COMPUTE_SUCCESS;
}

int main(int argc, char** argv) {
    // Find the STOP char and set argc to it
    int stopCharFound = 0;
    for (int i = 1; i < argc; i++) {
        if (argv[i][0] == STOP_CHAR) {
            argc = 3 * ((i - 1) / 3) + 1;
            stopCharFound = 1;
            break;
        }
    }
    if (!stopCharFound) {
        printf("No STOP char found (%c).\n", STOP_CHAR);
        return MAIN_NO_STOP_CHAR;
    }

    int ptr = 1;
    float res;
    printf("Hello world\n");
    while (ptr != argc) {
        // Compute
        float a = atof(argv[ptr + 1]);
        float b = atof(argv[ptr + 2]);
        printf("Op: %s\n", argv[ptr]);
        int err = compute2(&res, argv[ptr][0], a, b);

        // Handle error if any
        if (err != COMPUTE_SUCCESS) {
            printf("%s\n", computeVerbose(err));
            return err;
        }

        // Print result
        printf("%f %c %f = %f\n", a, argv[ptr][0], b, res);

        ptr += 3;
    }

    return MAIN_SUCCESS;
}

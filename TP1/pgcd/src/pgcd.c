#include <stdio.h>
#include <stdlib.h>

#define PGCD_SUCCESS 0
#define PGCD_FAILURE 1

int __pgcd__(int* res, int a, int b) {
    if (a == b) {
        if (res == NULL) {
            return PGCD_FAILURE;
        }
        *res = a;
        return PGCD_SUCCESS;
    } else if (a > b) {
        return __pgcd__(res, a - b, b);
    } else {
        return __pgcd__(res, a, b - a);
    }
}

// Doesn't check for max recursive calls
int pgcd(int* res, int a, int b) {
    a = (a > 0) ? a : -a;
    b = (b > 0) ? b : -b;
    return __pgcd__(res, a, b);
}

int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Please provide two numbers to compute their PGCD.\n");
        return 1;
    }

    int a = atoi(argv[1]);
    int b = atoi(argv[2]);

    int res;

    int err = pgcd(&res, a, b);
    if (err != PGCD_SUCCESS) {
        printf("Failed to compute PGCD value for %d and %d.\n", a, b);
        return 1;
    }

    printf("%d\n", res);

    return 0;
}

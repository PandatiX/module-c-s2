#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define POWER_SUCCESS 0
#define POWER_NULL_RES 1
#define POWER_OUT_OF_BOUNDS 2
#define POWER_OVERFLOW 3

char* powerVerbose(int code) {
    switch (code) {
        case POWER_SUCCESS:
            return "Everything worked fine.";
        case POWER_NULL_RES:
            return "The given res (int*) is set to NULL.";
        case POWER_OUT_OF_BOUNDS:
            return "The given values are out of bounds (x <= 0 or n < 0).";
        case POWER_OVERFLOW:
            return "The operation is not possible because of an overflow.";
        default:
            return "THIS ERROR CODE IS NOT HANDLED";
    }
}

int power(int* res, int n, int pow) {
    // Check pointer
    if (res == NULL) {
        return POWER_NULL_RES;
    }

    // Check bounds
    if (n <= 0 || pow < 0) {
        return POWER_OUT_OF_BOUNDS;
    }

    if (pow == 0) {
        *res = 1;
        return POWER_SUCCESS;
    }

    // Compute n^pow
    *res = n;
    for (int i = 1; i < pow; i++) {
        if (*res > INT_MAX / pow) {
            return POWER_OVERFLOW;
        }
        *res *= n;
    }

    return POWER_SUCCESS;
}

int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Did not find the expected args as [bin X N] in CLI.\n");
        return 1;
    }

    int x = atoi(argv[1]);
    int n = atoi(argv[2]);

    int res, err;
    err = power(&res, x, n);

    if (err != POWER_SUCCESS) {
        printf("Failed to compute %d to the power of %d. %s\n", x, n, powerVerbose(err));
        return 1;
    }

    printf("%d ^ %d = %d.\n", x, n, res);

    return 0;
}
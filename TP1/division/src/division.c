#include <stdio.h>

void print(float a) {
    printf("%f\n", a);
}

void div1() {
    float X = 10 / 3;          // 10 / 3 ~ 3.33... but 10 and 3 are int, so the return is an int, then we cast it to a float
    int iX = (int)X;           // Casting a float to an int will loose data (thanks we don't have any decimals)
    printf("%d\n", iX); // 3
}

void div2() {
    float X = 10.0 / 3; // 10.0 / 3 ~ 3.33... but 10.0 is float, 3 is int, so the return is a float
    print(X);           // 3.333333 (here only 6 decimals because of float size)
}

void div3() {
    float X = 10.0 / 3.0; // 10.0 / 3 ~ 3.33... but 10.0 and 3.0 are float, so the return is a float
    print(X);            // 3.333333 (here only 6 decimals because of float size)
}

void div4() {
    float X = (float) 10 / 3; // 10 is int, cast to float, then it's the same as div2
    print(X);
}

void div5() {
    float X = ((float) 10) / 3; // See div4
    print(X);
}

void div6() {
    float X = ((float) 10) / 3; // See div4
    printf("%.1f\n", X); // Print 1 decimal
    printf("%.5f\n", X); // Print 4 decimals
}

int main() {
    div1();
    div2();
    div3();
    div4();
    div5();
    div6();

    return 0;
}

#include <stdio.h>

void print(int a) {
    printf("%d\n", a);
}

int main() {
    int X = -3 + 4 * 5 - 6;
    print(X);

    X = (-3 + 4) * 5 - 6;
    print(X);

    X = -3 + (4 * 5) - 6;
    print(X);

    X = -3 + 4 * (5 - 6);
    print(X);

    return 0;
}
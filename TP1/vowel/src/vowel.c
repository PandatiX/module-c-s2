#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN_STR 1024

#define TRIGGER_CHAR '#'

#define COUNT_VOWELS_SUCCESS 0
#define COUNT_VOWELS_NULL 1

int isAVowel(char c) {
    return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y');
}

int countVowels(int* res, char* str) {
    if (res == NULL || str == NULL) {
        return COUNT_VOWELS_NULL;
    }

    *res = 0;
    int len = strnlen(str, MAX_LEN_STR);
    for (int i = 0; i < len; i++) {
        if (isAVowel(str[i])) {
            ++*res; // ++a and not a++ because res is a pointer so it won't increase the pointer (see pointer arithmetic)
        }
    }

    return COUNT_VOWELS_SUCCESS;
}

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Did not find one string in CLI args.\n");
        return 1;
    }

    // Find str len to the trigger char
    int triggerLen = -1;
    int len = strnlen(argv[1], MAX_LEN_STR);
    for (int i = 0; i < len; i++) {
        if (argv[1][i] == TRIGGER_CHAR) {
            triggerLen = i;
            break;
        }
    }

    if (triggerLen == -1) {
        printf("Did not find trigger char (%c) in provided string.\n", TRIGGER_CHAR);
        return 1;
    }

    char* str = (char*)malloc(sizeof(char) * triggerLen);
    strncpy(str, argv[1], triggerLen);

    int vowels, err;
    err = countVowels(&vowels, str);

    if (err != COUNT_VOWELS_SUCCESS) {
        printf("Failed to count vowels.\n");
        return 1;
    }

    printf("There are %d vowels in [%s].\n", vowels, str);

    return 0;
}
#include <stdio.h>

void mod1() {
    int X = 5 % 2;
    printf("%d\n", X);
}

void mod2() {
    // float X = 5.0 % 2;          // Can't work because % works on int
    // printf("%f\n", X);
}

int main() {
    mod1();
    mod2();

    return 0;
}
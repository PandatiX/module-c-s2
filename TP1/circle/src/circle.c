#include <stdio.h>
#include <stdlib.h>

#define PI 3.14159 // incomplete: PI should be 3.14159265358979323846 as defined in math lib as M_PI

int main(int argc, char** argv) {
    if (argc != 2) {
        return 1;
    }
    float radius = atof(argv[1]);

    printf("Radius: %f\n", radius);
    printf("Circumference: %f\n", 2 * PI * radius);
    printf("Area: %f\n", PI * radius * radius);

    return 0;
}
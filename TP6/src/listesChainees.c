#include "listesChainees.h"

#define regexPattern "[1-9][0-9][0-9][0-9]"

// Possibilités de choix pour le menu
#define CHOICE_PRINT_PERSON 1
#define CHOICE_CREATE_PERSON 2
#define CHOICE_DELETE_PERSON 3
#define CHOICE_CHANGE_SECRET 4
#define CHOICE_SIMULATION 5
#define CHOICE_SAVE_IN_FILE 6
#define CHOICE_READ_FROM_FILE 7
#define CHOICE_LEAVE 8


void clearBuffer(char* string, int size_of_str) {
    int length = strlen(string);
    int place;
    if (length + 1 < size_of_str) {
        place = length - 1;
        if (place >= 0) string[place] = '\0';
    } else if(string[size_of_str - 2] == '\n') {
        place = length - 1;
        if (place >= 0) string[place] = '\0';
    } else {
        while (getchar() != '\n');
    }
}

void clearFgets(char *user_entry, int size_of_str) {
    if (fgets(user_entry, size_of_str, stdin) == NULL) return;
    clearBuffer(user_entry, size_of_str);
}

void changeTimeSpace(char *buffer, char *time) {
    int i, len;
    len = strlen(time);
    for(i = 0; i < len; i++) {
        if(time[i] == ' ') {
            buffer[i] = '_';
        }
        else {
            buffer[i] = time[i];
        }
    }
}

void changeTime(char *time) {
    int i, len;
    len = strlen(time);
    for(i = 0; i < len; i++) {
        if(time[i] == '_')
            time[i] = ' ';
    }
}

int checkInput(char *user_entry) {
    if((user_entry[0] > 48 && user_entry[0] < 58) && user_entry[0] - '0' < 10) {
        return 0;
    }
    return 1;
}

void freeAll(node *head) {
    node *tmp_node;
    while(head != NULL && head->personne != NULL) {
        tmp_node = head;
        head = head->next;
        free(tmp_node->personne);
        free(tmp_node);
    }
}

void printPeople(node *head) {
    int cpt = 1;
    if (head->personne == NULL) {
        fputs("+++++++ TABLEAU VIDE +++++++\n",stdout);
        fflush(stdout);
    } else {
        printf("\n\n--------------------- Affichage des personnes -----------------------\n");
        while(head != NULL && head->personne != NULL) {
            printf("\nPERSONNE %d",cpt);
            printf("\n      Prénom : %s", head->personne->firstname);
            printf("\n      Nom : %s", head->personne->lastname);
            printf("\n      Badge :  %d", head->personne->badge);
            printf("\n      Code secret : %s", head->personne->secret);
            printf("\n      Heure de passage : %s\n", ctime(&head->personne->time));
            head = head->next;
            cpt ++;
        }
    }
}

void printPeopleReverse(node *queue) {
    int cpt = 1;
    if (queue->personne == NULL) {
        fputs("+++++++ TABLEAU VIDE +++++++\n",stdout);
        fflush(stdout);
    } else {
        printf("\n\n------------------- Affichage des personnes de manière inversée ----------------\n");
        while(queue != NULL && queue->personne != NULL) {
            printf("\nPERSONNE %d",cpt);
            printf("\n      Prénom : %s", queue->personne->firstname);
            printf("\n      Nom : %s", queue->personne->lastname);
            printf("\n      Badge :  %d", queue->personne->badge);
            printf("\n      Code secret : %s", queue->personne->secret);
            printf("\n      Heure de passage : %s\n", ctime(&queue->personne->time));
            queue = queue->prev;
            cpt ++;
        }
    }
}

int getPass(int badge, node *head) {
    char *buf;
    int res;
    printf("Mot de passe que vous souhaitez : %d\n", badge);
    printf("Mot de passe :     ");
    buf = (char *) malloc(20 * sizeof(char));
    memset(buf, 0, 20);
    clearFgets(buf, 20);
    res = strncmp(buf, head->personne->secret, 20);
    free(buf);
    return res;
}

int generateBadge() {
    int randomFd;
    int myRandomInteger;
    size_t randomDataLen = 0;

    randomFd = open("/dev/random", O_RDONLY);

    // Parcours du tableau pour afficher la liste avant le tri
    if (randomFd < 0) {
        printf("[!] Erreur à l'ouverture de /dev/random");
        exit(EXIT_FAILURE);
    }
    ssize_t result = read(randomFd, ((char*)&myRandomInteger) + randomDataLen, (sizeof myRandomInteger) - randomDataLen);
    if (result < 0) {
        printf("[!] Erreur à l'ouverture de /dev/random");
    }
    if (myRandomInteger < 0) myRandomInteger = myRandomInteger * (-1);
    myRandomInteger = myRandomInteger%9999;
    if (myRandomInteger < 1000) myRandomInteger = myRandomInteger + 1000;
    close(randomFd);
    return myRandomInteger;
}

int personGenerator(person *newPeople) {
    memcpy(newPeople->lastname, "person", 7);
    memcpy(newPeople->firstname, "person", 7);
    newPeople->badge = generateBadge();
    strncpy(newPeople->secret, "secret", 12);
    newPeople->time = time(0);

    if(newPeople->time == -1) {
        perror("    [!!!] Impossible d'ajouter le champs heure");
        return 1;
    }
    printf("%s", ctime(&newPeople->time));
    return 0;
}

int checkBadge(char *badge) {
    regex_t regex;
    int res;
    res = regcomp(&regex, regexPattern, 0);

    if(res) {
        regfree(&regex);
        perror("    [!!!] Erreur dans la regex\n");
        return 1;
    }
    res = regexec(&regex, badge, 0, NULL, 0);
    regfree(&regex);
    return res;
}

int getBadge() {
    int tmpBadge;
    char *buf;
    buf = (char *) malloc(5 * sizeof(char));

    printf("Numéro de badge :     ");
    clearFgets(buf, 5);

    if(checkBadge(buf) == REG_NOMATCH) {
        printf("    [!!!] Format de badge invalide ! %s\n",regexPattern);
        return 0;
    }
    tmpBadge = atoi(buf);
    free(buf);
    return tmpBadge;
}

void cleanPeople(person *newPeople) {
    memset(newPeople->secret, 0, 20);
    memset(newPeople->lastname, 0, 30);
    memset(newPeople->firstname, 0, 30);
}

int peopleGeneratorUser(person *newPeople) {
    char *buf;
    int tmpBadge, length;
    cleanPeople(newPeople);
    buf = (char *) malloc(30 * sizeof(char));
    memset(buf, 0, 30);
    printf("Enter first name =>     ");
    clearFgets(buf, 30);
    memcpy(newPeople->firstname, buf, strlen(buf));
    memset(buf, 0, 30);
    printf("Enter last name =>     ");
    clearFgets(buf, 30);
    memcpy(newPeople->lastname, buf, strlen(buf));
    tmpBadge = getBadge();
    if (!tmpBadge) {
        return 1;
    } else {
        newPeople->badge = tmpBadge;
    }
    memset(buf, 0,  30);
    printf("Enter your password =>     ");
    clearFgets(buf, 20);
    length = strlen(buf);
    memcpy(newPeople->secret, buf, length);
    newPeople->time = time(0);
    free(buf);
    return 0;
}

node *addList(node *head, person *personne) {
    node *tmp;
    tmp = (node *) malloc(sizeof(node));
    tmp->personne = personne;
    while(head->next != NULL) {
        head = head->next;
    }
    tmp->next = NULL;
    head->next = tmp;
    tmp->prev = head;
    return head->next;
}

int changeSecret(node *head) {
    int tmpBadge, isBadgeExist = 0;
    char *buf;
    tmpBadge = getBadge();
    if(!tmpBadge) {
        return 1;
    }
    while(head != NULL && head->personne != NULL) {
        if (head->personne->badge == tmpBadge) {
            isBadgeExist = 1;
            break;
        }
        head = head->next;
    }
    if(!isBadgeExist) {
        printf("    [!!!] %d, ce numéro n'est pas enregistré\n", tmpBadge);
        return 1;
    }
    if(getPass(tmpBadge, head)) {
        printf("    [!!!] Mauvais mot de passe\n");
        return 1;
    }
    buf = (char *) malloc(sizeof(char) * 20);
    memset(buf, 0 , 20);
    clearFgets(buf, 20);
    memset(head->personne->secret, 0, 20);
    memcpy(head->personne->secret, buf, strlen(buf));
    free(buf);
    return 0;
}

int accessSimulation(node *head) {
    int tmpBadge, isBadgeExist = 0;
    tmpBadge = getBadge();
    if(!tmpBadge)
        return 1;
    while(head != NULL && head->personne != NULL) {
        if (head->personne->badge == tmpBadge) {
            isBadgeExist = 1;
            break;
        }
        head = head->next;
    }
    if(!isBadgeExist) {
        printf("    [!!!] %d, ce numéro n'est pas enregistré\n", tmpBadge);
        return 1;
    }
    if(getPass(tmpBadge, head)) {
        printf("    [!!!] Mauvais mot de passe\n");
        return 1;
    }
    head->personne->time = time(0);
    printf("Nom : %30s", head->personne->firstname);
    printf("Heure : %15s", ctime(&head->personne->time));
    return 0;
}

node *deleteLast(node *queue) {
    node *tmp_node;
    tmp_node = queue;
    queue->prev->next = NULL;
    queue = queue->prev;
    free(tmp_node->personne);
    free(tmp_node);
    return queue;
}

node *deleteFirst(node *head) {
    node *tmp_node;
    tmp_node = head;
    if(head->next != NULL) {
        head->next->prev = NULL;
        head = head->next;
    } else {
        free(tmp_node->personne);
        free(tmp_node);
        return NULL;
    }
    free(tmp_node->personne);
    free(tmp_node);
    return head;
}

int deleteWithBadge(node *head) {
    int tmpBadge, isBadgeExist = 0;
    tmpBadge = getBadge();
    if(!tmpBadge)
        return 3;
    while(head != NULL && head->personne != NULL) {
        if (head->personne->badge == tmpBadge) {
            isBadgeExist = 1;
            break;
        }
        head = head->next;
    }
    if(!isBadgeExist) {
        printf("    [!!!] %d, ce numéro n'est pas enregistré\n", tmpBadge);
        return 3;
    }
    if(head->prev == NULL) {
        return 0;
    }
    if(head->next == NULL) {
        return 1;
    }
    head->prev->next = head->next;
    head->next->prev = head->prev;
    free(head);
    return 2;
}

int saveIntoFile(node *head) {
    char *buf;
    FILE *file;
    buf = (char *) malloc(sizeof(char) * 50);
    memset(buf, 0, 50);
    printf("Nom du fichier :     ");
    clearFgets(buf, 50);
    file = fopen(buf, "w");

    if (file == NULL) {
        printf("    [!!!] %s ne peut être ouvert\n", buf);
        free(buf);
        return 1;
    }
    while (head != NULL && head->personne != NULL) {
        changeTimeSpace(buf, ctime(&head->personne->time));
        fprintf(file, "%s %s %d %s %s", head->personne->firstname, head->personne->lastname,
                head->personne->badge, head->personne->secret, buf);
        head = head->next;
    }
    if (fclose(file)) {
        printf("    [!!!] %s ne peut êter fermé\n", buf);
        free(buf);
        return 1;
    }
    free(buf);
    return 0;
}

node *reverse(node *head) {
    head->next = NULL;
    while(head != NULL && head->personne != NULL) {
        if(head->prev != NULL) {
            head->prev->next = head;
            head = head->prev;
        }
        else {
            break;
        }
    }
    return head;
}

node *readRegisteredPeople(node *head) {
    char *buf;
    FILE *file;
    int res, cpt = 0;
    struct tm *tmp_time;
    node *tmp_node;
    buf = (char *) malloc(sizeof(char) * 50);
    memset(buf, 0, 50);
    printf("Nom du fichier :     ");
    clearFgets(buf, 50);
    file = fopen(buf, "r");
    if (file == NULL) {
        printf("    [!!!] %s ne peut être ouvert\n", buf);
        free(buf);
        return head;
    }
    tmp_time = (struct tm *) malloc(sizeof(struct tm));
    tmp_node = (node *) malloc(sizeof(node));
    tmp_node->personne = (person *) malloc(sizeof(person));
    while(1) {
        memset(buf, 0, 50);
        res = fscanf(file, "%30s %30s %4d %20s %26s", tmp_node->personne->firstname, tmp_node->personne->lastname,
                     &tmp_node->personne->badge, tmp_node->personne->secret, buf);
        if(res == EOF) {
            free(buf);
            free(tmp_time);
            return head;
        }
        changeTime(buf);
        strptime(buf, "%a %b %e %H:%M:%S %Y", tmp_time);
        tmp_node->personne->time = mktime(tmp_time);
        tmp_node->next = (node *) malloc(sizeof(node));
        if(!cpt) {
            tmp_node->prev = NULL;
        } else {
            tmp_node->prev = head;
        }
        cpt++;
        head = tmp_node;
        tmp_node = (node *) malloc(sizeof(node));
        tmp_node->personne = (person *) malloc(sizeof(person));
    }
}

node *addPersonList(node *head) {
    char user_entry[2];
    int choice, success = 1;
    person *newPeople;
    if(head == NULL) {
        head = (node *) malloc(sizeof(node));
        head->prev = NULL;
        head->next = NULL;
        head->personne = NULL;
    }
    while(success) {
        printf("    [1] Pour générer une personne\n");
        printf("    [2 à 9] Pour créer une personne\n");
        printf("Votre choix :     ");
        clearFgets(user_entry, 2);
        success = checkInput(user_entry);
    }
    choice = atoi(user_entry);
    newPeople = (person *) malloc(sizeof(person));
    if(choice == 1) {
        success = personGenerator(newPeople);
    } else {
        success = peopleGeneratorUser(newPeople);
    }
    if(!success) {
        printf("    [!] Personne ajoutée\n");
    } else {
        printf("    [!!!] Création impossible\n");
        return NULL;
    }
    if (head->personne == NULL) {
        head->personne = newPeople;
    } else {
        head = addList(head, newPeople);
    }
    return head;
}

void menu() {
    printf("Choisissez l'action que vous souhaitez réaliser : \n    [%d]  Pour afficher toutes les personnes\n    [%d]  Pour créer une nouvelle personne\n    [%d]  Pour supprimer une personne\n    [%d]  Pour changer un code secret\n    [%d]  Pour simuler la gestion des accès\n    [%d]  Pour effectuer une sauvegarde dans un fichier\n    [%d]  Pour charger une base de personnes depuis un fichier\n    [%d]  Pour quitter\n", CHOICE_PRINT_PERSON, CHOICE_CREATE_PERSON, CHOICE_DELETE_PERSON, CHOICE_CHANGE_SECRET, CHOICE_SIMULATION, CHOICE_SAVE_IN_FILE, CHOICE_READ_FROM_FILE, CHOICE_LEAVE);
}

int main(void) {
    char user_entry[2];
    int choice, res;
    node *head = (node *) malloc(sizeof(node));
    node *queue;
    head->personne = NULL;
    head->prev = NULL;
    head->next = NULL;
    queue = head;
    while(1) {
        menu();
        printf("Votre choix :     ");
        clearFgets(user_entry, 2);
        choice = atoi(user_entry);
        switch (choice) {
            case CHOICE_PRINT_PERSON:
                printPeople(head);
                printPeopleReverse(queue);
                break;
            case CHOICE_CREATE_PERSON:
                queue = addPersonList(head);
                break;
            case CHOICE_DELETE_PERSON:
                res = deleteWithBadge(head);
                if(res == 1) {
                    queue = deleteLast(queue);
                    printf("     [!] Personne supprimée\n\n");
                }
                else if(res == 0) {
                    head = deleteFirst(head);
                    if(head == NULL) {
                        queue = NULL;
                    }
                    printf("     [!] Personne supprimée\n\n");
                }
                else if(res == 2)
                    printf("     [!] Personne supprimée\n\n");
                else
                    printf("Person hasn't been deleted ! \n\n");
                break;
            case CHOICE_CHANGE_SECRET:
                if(changeSecret(head))
                    printf("\n     [!] Code secret changé\n");
                break;
            case CHOICE_SIMULATION:
                if(accessSimulation(head))
                    printf("    [!!!] Access denied\n");
                else
                    printf("    [!!!] Access granted\n");
                break;
            case CHOICE_SAVE_IN_FILE:
                if(!saveIntoFile(head))
                    printf("     [!] Personnes enregistrées dans le fichier\n");
                else
                    printf("     [!!!] Action impossible\n");
                break;
            case CHOICE_READ_FROM_FILE:
                queue = readRegisteredPeople(head);
                head = reverse(queue);
                break;
            case CHOICE_LEAVE:
                freeAll(head);
                exit(EXIT_SUCCESS);
            default:
                printf("     [!!!] Votre choix ne correspond pas veuillez respecter les consignes de lancement !\n");
        }
    }
    return 0;
}

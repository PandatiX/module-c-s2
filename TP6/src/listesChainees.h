#define _XOPEN_SOURCE

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <regex.h>
#include <fcntl.h>
#include <unistd.h>

typedef struct person {
    char firstname[30];
    char lastname[30];
    int badge;
    char secret[20];
    time_t time;
} person;

typedef struct node {
    struct person *personne;
    struct node *next;
    struct node *prev;
} node;

void printPeople(node *head);
void printPeopleReverse(node *queue);
node *addPersonList(node *head);
node *addList(node *head, person *personne);
int deleteWithBadge(node *head);
int accessSimulation(node *head);
int saveIntoFile(node *head);
node *readRegisteredPeople(node *head);
void clearBuffer(char* string, int size_of_str);
void clearFgets(char *user_entry, int size_of_str);
int checkInput(char *user_entry);
int checkBadge(char *badge);
node *deleteFirst(node *head);
node *deleteLast(node *queue);
node *reverse(node *head);
void freeAll(node *head);
void changeTime(char *time);
void changeTimeSpace(char *buffer, char *time);
int peopleGeneratorUser(person *newPeople);
int personGenerator(person *newPeople);
int generateBadge();
void cleanPeople(person *newPeople);
int getBadge();
int getPass(int badge, node *head);
void menu();
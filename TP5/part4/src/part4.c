#include <stdio.h>

void echanger(int a, int b) {
    int tmp;
    tmp = a;
    a = b;
    b = tmp;
}

void echangerPtr(int* pa, int* pb) {
    if (pa == NULL || pb == NULL) {
        return;
    }

    int tmp;
    tmp = *pa;  // 1
    *pa = *pb;  // 2
    *pb = tmp;  // 3
}

int main() {
    int x, y;
    x = 12;
    y = 34;
    
    printf("Avant échange : x = %d ; y = %d\n", x, y);
    
    echangerPtr(&x, &y);
    
    printf("Après échange : x = %d ; y = %d\n", x, y);

    return 0;
}

/*
1. En l'état, la fonction echanger prend a et b par copie, ce qui fait
   que les modifications n'ont pas d'effet.
   Schématiquement, l'appel à cette fonction procède comme s'en suit.
   (les adresses sont évidemment fausses).

   Adresses Mémoire
   0001      x = 12
   0002      y = 34
   ...
   000d      a = 12
   000e      b = 34
   000f      tmp

   puis les actions ont lieu sur a, b et tmp, mais pas sur x et y.

3. La fonction corrigée d'échange en passant par les pointeurs des valeurs
   à échanger fonctionne comme s'en suit.

   Dans main:
     x      y
   +----+ +----+
   | 12 | | 34 |
   +----+ +----+

   On appelle alors echangerPtr(&x et &y), soit:
            echangerPtr( , )
                        A A
     x      &x----------+ |
   +----+  +---+          |
   | 12 |<-+-o |          |
   +----+  +---+          |
                          |
     y      &y------------+
   +----+  +---+
   | 34 |<-+-o |
   +----+  +---+

   Dans echangerPtr, on opère alors l'échange sur les valeurs pa et pb.
   Etape 1:
    tmp   pa         pb
   +---+ +---+      +---+
   | x | | o-+->(x) | o-+->(y)
   +---+ +---+      +---+

   Etape 2:
    tmp   pa         pb
   +---+ +---+      +---+
   | x | | o-+->(y) | o-+->(y)
   +---+ +---+      +---+

   Etape 3:
    tmp   pa         pb
   +---+ +---+      +---+
   | x | | o-+->(y) | o-+->(x)
   +---+ +---+      +---+

   On a donc bien échangé en mémoire les valeurs de x et y depuis echangerPtr.
*/
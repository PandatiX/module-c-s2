#include <stdio.h>

void echangerPointeurs(int **ppa, int **ppb) {
    if (ppa == NULL || ppb == NULL) {
        return;
    }

    int *tmp;
    tmp = *ppa;
    *ppa = *ppb;
    *ppb = tmp;
}

int main() {
    int x = 12, y = 34;
    int *px = &x, *py = &y;

    printf("Avant échange: x = %d, *px = %d ; y = %d, *py = %d\n", x , *px, y, *py);

    echangerPointeurs(&px, &py);

    printf("Après échange: x = %d, *px = %d ; y = %d, *py = %d\n", x , *px, y, *py);

    return 0;
}

/*
Cette fois, echangerPointeurs fait encore plus beau que echangerPtr de part4.c:
on n'échange pas les valeurs, ce qui dans le cas d'un int n'est pas très grave mais
qui dans le cas de structures peut s'avérer couteux, mais directement leurs pointeurs
sur lesquels on travaille.

C'est l'opération la plus simple et efficace que l'on puisse faire d'un point de vue
de la mémoire et de la rapidité de l'exécution du programme.

Le fonctionnement de la fonction échangerPointeurs est comme pour part4.c, sauf
qu'on ne travaille pas sur des int mais des int*.
Les int de base ne sont pas modifiés, seulement les pointeurs px et py.
*/

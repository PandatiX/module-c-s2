#include <stdlib.h>
#include <stdio.h>

#define N 3

typedef struct Maillon {
    int x;
    struct Maillon * suiv;
} Maillon;

int main() {
    Maillon lc;
    lc.x = 1;
    
    printf("Valeur du champs x = %d\n\n", lc.x);
    
    lc.suiv = (Maillon*) malloc(sizeof(Maillon));
    lc.suiv->x = 2;
    
    printf("Valeur du champs x du deuxieme maillon = %d\n\n", lc.suiv->x);
    
    free(lc.suiv);
    return 0;
}

/*
Afin d'éviter d'avoir à représenter dans le stack le fonctionnement des maillons,
on représente ce qu'il se passe en omettant les adresses mémoires.

Ligne 12 on a donc:
   lc
+------+
|  0   |
| NULL |
+------+

Puis ligne 13:
   lc
+------+
|  13  |
| NULL |
+------+

Ligne 17 et 18, on construit un second maillon (qu'on nomme ici lc-new), on a donc:
  lc      lc-new
+----+   +------+
| 13 |   |  2   |
| o--+-->| NULL |
+----+   +------+
*/

#include <stdlib.h>
#include <stdio.h>

const int N = 3;

typedef struct Maillon {
    int id;
    struct Maillon* next;
} Maillon;

#define MAILLON_SUCCESS 0
#define MAILLON_NULL 1
#define MAILLON_NEXT_NOT_NULL 2
#define MAILLON_NOT_FOUND 3

// VerboseMaillonErrCode returns a description of a MAILLON_X error code.
const char* VerboseMaillonErrCode(int code) {
    switch (code) {
        case MAILLON_SUCCESS:
            return "Everything worked fine.";
        case MAILLON_NULL:
            return "The specified maillon is NULL.";
        case MAILLON_NEXT_NOT_NULL:
            return "The specified maillon already have a next maillon.";
        case MAILLON_NOT_FOUND:
            return "The specified ID was not found in the chained list from the specified maillon.";
        default:
            return "THIS ERROR CODE IS NOT HANDLED";
    }
}

#define checkNull(m) if (m == NULL) { return MAILLON_NULL; }

// PushBack adds a Maillon after the specified one.
// Returns a MAILLON_X err code.
int PushBack(Maillon** start) {
    checkNull(start);
    checkNull(*start);

    if ((*start)->next != NULL) {
        return MAILLON_NEXT_NOT_NULL;
    }

    (*start)->next = (Maillon*)malloc(sizeof(Maillon));
    return MAILLON_SUCCESS;
}

// PushBackN adds N Maillon next to the specified one.
// Returns a MAILLON_X err code.
int PushBackN(Maillon** start, int n) {
    checkNull(start);
    checkNull(*start);

    // Build chained list
    Maillon** it = start;
    int i;
    for (i = 0; i < n; i++) {
        int err = PushBack(it);
        if (err != MAILLON_SUCCESS) {
            return err;
        }

        (*it)->id = i;
        it = &(*it)->next;
    }

    // Setup the last maillon
    free((*it)->next);
    (*it)->id = i;
    (*it)->next = NULL;

    return MAILLON_SUCCESS;
}

// RemoveID removes a maillon which matches the specified id form
// the start Maillon.
// Returns a MAILLON_X err code.
int RemoveID(Maillon** start, int id) {
    checkNull(start);
    checkNull(*start);

    Maillon* it = *start;

    // If the specified maillon to delete is the start, then
    // set its pointer to its next one to avoid loosing the chained
    // list in memory.
    if (it->id == id) {
        *start = (*start)->next;
        free(it);
        return MAILLON_SUCCESS;
    }

    Maillon *itNext = it->next;
    while (itNext != NULL) {
        if (itNext->id == id) {
            // Delete the Maillon
            it->next = itNext->next;

            if (itNext->next == NULL) {
                // If last one free it
                free(itNext);
            }
        }

        it = itNext;
        itNext = itNext->next;
    }

    return MAILLON_SUCCESS;
}

// PrintMaillons prints in stdout the chained list from the
// specified Maillon.
// Returns a MAILLON_X err code.
int PrintMaillons(Maillon** start) {
    checkNull(start);
    checkNull(*start);

    printf("--- MAILLONS ---\n");

    Maillon** it = start;
    while (1) {
        printf("%d\n", (*it)->id);
        if ((*it)->next == NULL) {
            break;
        }
        it = &(*it)->next;
    }

    printf("--- END ---\n");

    return MAILLON_SUCCESS;
}

// FreeMaillons takes a pointer to a Maillon and deletes all
// the following and itself, setting them NULL.
// Returns a MAILLON_X err code.
int FreeMaillons(Maillon** start) {
    checkNull(start);

    if (*start != NULL) {
        int err = FreeMaillons(&(*start)->next);
        if (err != MAILLON_SUCCESS) {
            return err;
        }
    }
    free(*start);
    *start = NULL;

    return MAILLON_SUCCESS;
}

int main() {
    Maillon* m = (Maillon*)malloc(sizeof(Maillon));

    PrintMaillons(&m);

    PushBackN(&m, N);
    PrintMaillons(&m);

    RemoveID(&m, 0);
    PrintMaillons(&m);

    FreeMaillons(&m);
    return 0;
}

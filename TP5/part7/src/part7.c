#include <stdio.h>
#include <stdlib.h>

#define N 3

typedef struct maillon {
    int x;
    struct maillon * suiv;
} maillon;

int main() {
    maillon *lc;    // 1
    maillon *tete;  // 1
    int cpt;        // 1
    
    // Init des maillons
    lc = (maillon *) malloc(sizeof(maillon));   // 2
    tete = lc;                                  // 3
    
    // Creation des maillons en fin de liste
    for(cpt = 1; cpt < N; cpt++) {
        lc->suiv = (maillon *) malloc(sizeof(maillon)); // 4
        lc = lc->suiv;                                  // 4
    }
    lc->suiv = NULL;                                    // 4
    
    cpt = 0;    // 5
    lc = tete;  // 5
    
    // Remplissage des valeurs de chacun des maillons
    while (lc != NULL) {
        lc->x = cpt;    // 6
        cpt++;          // 6
        lc = lc->suiv;  // 6
    }
    
    lc = tete;
    while (lc != NULL) {
        printf("Valeur du champs courant = %d \n",lc->x);
        printf("Adresse maillon courant= %X et du suivant %X\n", lc, lc->suiv);
        lc = lc->suiv; //passe au suivant        
    }

    // Et on oublie de nettoyer la mémoire parce qu'on a trop de RAM :)

    return 0;
}

/*
Etape 1:
 tete    lc
+---+   +---+
| o-+-/ | o-+-/
+---+   +---+

Etape 2:
tete     lc
+---+   +---+   +------+
| o-+-/ | o-+-->|  ?   |
+---+   +---+   | NULL |
                +------+

Etape 3:
tete                lc
+---+   +------+   +---+
| o-+-->|  ?   |<--+-o |
+---+   | NULL |   +---+
        +------+

Etape 4:
tete                           lc
+---+   +------+   +------+   +---+
| o-+-->|  ?   |   |  ?   |<--+-o |
+---+   | o----+-->| NULL |   +---+
        +------+   +------+

Puis

tete                                      lc
+---+   +------+   +------+   +------+   +---+
| o-+-->|  ?   |   |  ?   |   |  ?   |<--+-o |
+---+   |  o---+-->|  o---+-->| NULL |   +---+
        +------+   +------+   +------+

Et ainsi de suite...

Etape 5:
tete                                               cpt  
+---+   +------+   +------+   +------+   +------+ +---+
| o-+-->|  0   |   |  ?   |   |  ?   |   |  ?   | | 0 |
+---+   |  o---+-->|  o---+-->|  o---+-->| NULL | +---+
        +------+   +------+   +------+   +------+
 lc        A
+---+      |
| o-+------+
+---+

Puis

tete                                               cpt  
+---+   +------+   +------+   +------+   +------+ +---+
| o-+-->|  0   |   |  1   |   |  ?   |   |  ?   | | 1 |
+---+   |  o---+-->|  o---+-->|  o---+-->| NULL | +---+
        +------+   +------+   +------+   +------+
 lc                   A
+---+                 |
| o-+-----------------+
+---+

Et ainsi de suite...

On aboutit donc avec la situation suivante (à la fin des printf).
tete                                               cpt   lc
+---+   +------+   +------+   +------+   +------+ +---+ +---+
| o-+-->|  0   |   |  1   |   |  2   |   |  3   | | 3 | | o-+-/
+---+   |  o---+-->|  o---+-->|  o---+-->| NULL | +---+ +---+
        +------+   +------+   +------+   +------+
*/

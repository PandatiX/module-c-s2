#include <stdio.h>
#define N 5

int main() {
    int i;
    int t[N] = {10, 20, 30, 40, 50};
    for (i = 0; i < N; i++)
        printf("t[%d] = %d; &t[%d] = %u\n", i, t[i], i, &t[i]);

    return 0;
}

/*
A l'exécution du programme, on relève dans stdout:
t[0] = 10; &t[0] = 774810224
t[1] = 20; &t[1] = 774810228
t[2] = 30; &t[2] = 774810232
t[3] = 40; &t[3] = 774810236
t[4] = 50; &t[4] = 774810240

Ce qui signifie que t (un tableau) est construit commet s'en suit.

Adresses          Mémoire
             +---------------+
774810224 -> | t = t[0] = 10 |
             +---------------+
774810228 -> |   t[1] = 20   |
             +---------------+
774810232 -> |   t[2] = 30   |
             +---------------+
774810236 -> |   t[3] = 40   |
             +---------------+
774810240 -> |   t[4] = 50   |
             +---------------+

On note que chaque élément de t se suit dans la mémoire,
et que ceci ne dépend pas des valeurs dans t (ce qui est plutôt logique
compte tenu du fonctionnement de la mémoire d'un ordinateur qui dépend
des types et non des valeurs).
*/

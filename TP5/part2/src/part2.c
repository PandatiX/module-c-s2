#include <stdlib.h>
#include <stdio.h>

int main() {
    int *px;
    px = (int *) malloc (sizeof(int));
    
    *px = 1;
    printf("Adresse de px : %X\n", &px);
    printf("Adresse pointee par le pointeur px = %X\n", px);
    printf("Valeur memorisee a l'adresse pointee par le pointeur px = %d\n\n", *px);

    free(px);
    return 0;
}

/*
Adresse de px : 1015D2B8
Adresse pointee par le pointeur px = 9AC362A0
Valeur memorisee a l'adresse pointee par le pointeur px = 1

On sait que px est un pointeur vers un entier (ligne 5) initialisé à NULL.
On lui alloue alors dynamiquement un espace en mémoire (stack) pour un entier.
On lui donne la valeur 1 (ligne 8) grâce à l'opérateur de déréférence.

On a alors un stack comme s'en suit, avec lequel on joue.
    Adresses        Mémoire
                  +----------+
px = 9AC362A0 ->  | *px = 1  | (adresse issue du malloc ligne 6)
                  +----------+
...               |   ...    |
                  +----------+
&px = 1015D2B8 -> | 9AC362A0 | (adress de px de la ligne 5)
                  +----------+
*/